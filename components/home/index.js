import Search from "./search";
import Latest from "./latest";
import Blog from "./blog";

const Index = () => (
    <div>
        <Search/>
        <Latest/>
        <Blog/>
    </div>
);

export default Index
