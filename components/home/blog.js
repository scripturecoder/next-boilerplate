const Blog = () => (
    <section id="blog" className="custom-padding">
        <div className="container">
            <div className="main-heading text-center">
                <h2>Latest Articles</h2>
                <div className="slices"><span className="slice"></span><span className="slice"></span><span className="slice"></span>
                </div>
                <p>Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc
                    vulputate facilisis nisi
                    <br/>eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit</p>
            </div>

            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div className="blog-grid">
                        <div className="blog-image">
                            <img alt="blog-image1" className="img-responsive" src="images/blog/1.jpg"/>
                        </div>
                        <div className="blog-content">
                            <h5><a href="blog-detail.html">How to prevent my website from being scrolled
                                horizontally?</a></h5>
                            <ul className="post-meta">
                                <li>By Admin</li>
                                <li>Php</li>
                                <li>27 July 2016</li>
                            </ul>
                            <p>We can make table scrollable by adding table-responsive class to it, but how can we loop
                                it so that once the loop ends..</p>
                        </div>
                        <div className="blog-footer">
                            <ul className="like-comment">
                                <li><a href="#"><i className="icon-heart"></i>23</a>
                                </li>
                                <li><a href="#"><i className="icon-chat"></i>32</a>
                                </li>
                            </ul>
                            <a href="#" className="more-btn pull-right"><i className="fa fa-long-arrow-right"></i> MORE</a>
                        </div>
                    </div>
                </div>

                <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div className="blog-grid">
                        <div className="blog-image">
                            <img alt="blog-image1" className="img-responsive" src="images/blog/2.jpg"/>
                        </div>
                        <div className="blog-content">
                            <h5><a href="blog-detail.html">What's the best way to implement a 2D interval search in
                                C++?</a></h5>
                            <ul className="post-meta">
                                <li>By Admin</li>
                                <li>Php</li>
                                <li>27 July 2016</li>
                            </ul>
                            <p>We can make table scrollable by adding table-responsive class to it, but how can we loop
                                it so that once the loop ends..</p>
                        </div>
                        <div className="blog-footer">
                            <ul className="like-comment">
                                <li><a href="#"><i className="icon-heart"></i>23</a>
                                </li>
                                <li><a href="#"><i className="icon-chat"></i>32</a>
                                </li>
                            </ul>
                            <a href="#" className="more-btn pull-right"><i className="fa fa-long-arrow-right"></i> MORE</a>
                        </div>
                    </div>
                </div>

                <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div className="blog-grid">
                        <div className="blog-image">
                            <img alt="blog-image1" className="img-responsive" src="images/blog/3.jpg"/>
                        </div>
                        <div className="blog-content">
                            <h5><a href="blog-detail.html"> Access a list within an element of a Pandas DataFrame </a>
                            </h5>
                            <ul className="post-meta">
                                <li>By Admin</li>
                                <li>Php</li>
                                <li>27 July 2016</li>
                            </ul>
                            <p>We can make table scrollable by adding table-responsive class to it, but how can we loop
                                it so that once the loop ends..</p>
                        </div>
                        <div className="blog-footer">
                            <ul className="like-comment">
                                <li><a href="#"><i className="icon-heart"></i>23</a>
                                </li>
                                <li><a href="#"><i className="icon-chat"></i>32</a>
                                </li>
                            </ul>
                            <a href="#" className="more-btn pull-right"><i className="fa fa-long-arrow-right"></i> MORE</a>
                        </div>
                    </div>
                </div>

                <div className="clearfix"></div>
                <div className="text-center clearfix section-padding-40"><a href="javascript:void(0)" className="btn btn-lg btn-primary">View all
                    Blog Post</a>
                </div>
                <div className="clearfix"></div>
            </div>

        </div>
    </section>

);

export default Blog
