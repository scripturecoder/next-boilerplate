const Latest = () => (
    <section className="white question-tabs padding-bottom-80" id="latest-post">
        <div className="container">
            <div className="row">
                <div className="col-md-12 col-sm-12 col-xs-12">
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            <ul className="nav panel-tabs">
                                <li><a data-toggle="tab" href="#tab1"><i className="icofont icon-ribbon"/><span
                                    className="hidden-xs">Recent Questions</span></a>
                                </li>
                                <li><a data-toggle="tab" href="#tab2"><i className="icofont icon-layers"/><span
                                    className="hidden-xs">Popular Responses</span></a>
                                </li>
                                <li className="active"><a data-toggle="tab" href="#tab3"><i className="icofont icon-global"/><span
                                    className="hidden-xs">Recently Answered</span></a>
                                </li>
                                <li><a data-toggle="tab" href="#tab4"><i className="icofont icon-linegraph"/><span
                                    className="hidden-xs">No answers</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="panel-body">
                        <div className="tab-content">
                            <div id="tab1" className="tab-pane active">
                                <div className="listing-grid">
                                    <div className="row">
                                        <div className="col-md-2 col-sm-2 col-xs-12 hidden-xs">
                                            <a data-toggle="tooltip" data-placement="bottom"
                                               data-original-title="Martina Jaz" href="#">
                                                <img alt="" className="img-responsive center-block" src="images/authors/1.jpg"/>
                                            </a>
                                        </div>
                                        <div className="col-md-7 col-sm-8  col-xs-12">
                                            <h3><a href="#"> Php recursive function not working right</a></h3>
                                            <div className="listing-meta"><span><i className="fa fa-clock-o" aria-hidden="true"/>8 mintes ago</span>
                                                <span><i className="fa fa fa-eye" aria-hidden="true"/> 750 Views</span>
                                            </div>
                                        </div>
                                        <div className="col-md-3 col-sm-2 col-xs-12">
                                            <ul className="question-statistic">
                                                <li className="active"><a data-toggle="tooltip" data-placement="bottom"
                                                                          data-original-title="Answers"><span>2</span></a>
                                                </li>
                                                <li><a data-toggle="tooltip" data-placement="bottom"
                                                       data-original-title="Votes"><span>0</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-10 col-sm-10  col-xs-12">
                                            <p>On the other hand, we denounce with righteous indignation and dislike men
                                                who are so beguiled and demoralized by the charms of pleasure of the
                                                moment.</p>
                                            <div className="pull-right tagcloud"><a href="">Php</a> <a
                                                href="">recursive</a> <a href="">error</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab2" className="tab-pane">
                                <div className="listing-grid">
                                    <div className="row">
                                        <div className="col-md-2 col-sm-2 col-xs-12 hidden-xs">
                                            <a data-toggle="tooltip" data-placement="bottom"
                                               data-original-title="Martina Jaz" href="#">
                                                <img alt="" className="correct img-responsive center-block" src="images/authors/5.jpg"/>
                                            </a> <span className="tick2"> <i className="fa fa-check" aria-hidden="true"/> </span>
                                        </div>
                                        <div className="col-md-7 col-sm-8  col-xs-12">
                                            <h3><a href="#"> Php recursive function not working right</a></h3>
                                            <div className="listing-meta"><span><i className="fa fa-clock-o" aria-hidden="true"/>8 mintes ago</span>
                                                <span><i className="fa fa fa-eye" aria-hidden="true"/> 750 Views</span> <span><i className="fa fa-comment" aria-hidden="true"></i>50 Comment</span>
                                            </div>
                                        </div>
                                        <div className="col-md-3 col-sm-2 col-xs-12">
                                            <ul className="question-statistic">
                                                <li className="active"><a data-toggle="tooltip" data-placement="bottom"
                                                                          data-original-title="Answers"><span>2</span></a>
                                                </li>
                                                <li><a data-toggle="tooltip" data-placement="bottom"
                                                       data-original-title="Votes"><span>0</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-10 col-sm-10  col-xs-12">
                                            <p>On the other hand, we denounce with righteous indignation and dislike men
                                                who are so beguiled and demoralized by the charms of pleasure of the
                                                moment.</p>
                                            <div className="pull-right tagcloud"><a href="">Php</a> <a
                                                href="">recursive</a> <a href="">error</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab3" className="tab-pane">
                                <div className="listing-grid">
                                    <div className="row">
                                        <div className="col-md-2 col-sm-2 col-xs-12 hidden-xs">
                                            <a data-toggle="tooltip" data-placement="bottom"
                                               data-original-title="Martina Jaz" href="#">
                                                <img alt="" className="correct img-responsive center-block" src="images/authors/9.jpg"/>
                                            </a> <span className="tick2"> <i className="fa fa-check" aria-hidden="true"></i> </span>
                                        </div>
                                        <div className="col-md-7 col-sm-8  col-xs-12">
                                            <h3><a href="#"> Php recursive function not working right</a></h3>
                                            <div className="listing-meta"><span><i className="fa fa-clock-o" aria-hidden="true"></i>8 mintes ago</span>
                                                <span><i className="fa fa fa-eye" aria-hidden="true"></i> 750 Views</span>
                                            </div>
                                        </div>
                                        <div className="col-md-3 col-sm-2 col-xs-12">
                                            <ul className="question-statistic">
                                                <li className="active"><a data-toggle="tooltip" data-placement="bottom" data-original-title="Answers"><span>2</span></a>
                                                </li>
                                                <li><a data-toggle="tooltip" data-placement="bottom" data-original-title="Votes"><span>0</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-10 col-sm-10  col-xs-12">
                                            <p>On the other hand, we denounce with righteous indignation and dislike men
                                                who are so beguiled and demoralized by the charms of pleasure of the
                                                moment.</p>
                                            <div className="pull-right tagcloud"><a href="">Php</a> <a
                                                href="">recursive</a> <a href="">error</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab4" className="tab-pane">
                                <div className="listing-grid">
                                    <div className="row">
                                        <div className="col-md-2 col-sm-2 col-xs-12 hidden-xs">
                                            <a data-toggle="tooltip" data-placement="bottom"
                                               data-original-title="Martina Jaz" href="#">
                                                <img alt="" className=" img-responsive center-block" src="images/authors/5.jpg"/>
                                            </a>
                                        </div>
                                        <div className="col-md-7 col-sm-8  col-xs-12">
                                            <h3><a href="#"> Php recursive function not working right</a></h3>
                                            <div className="listing-meta"><span><i className="fa fa-clock-o" aria-hidden="true"/>8 mintes ago</span>
                                                <span><i className="fa fa fa-eye" aria-hidden="true"></i> 750 Views</span> <span><i className="fa fa-comment" aria-hidden="true"/>50 Comment</span>
                                            </div>
                                        </div>
                                        <div className="col-md-3 col-sm-2 col-xs-12">
                                            <ul className="question-statistic">
                                                <li className="active"><a data-toggle="tooltip" data-placement="bottom"
                                                                          data-original-title="Answers"><span>2</span></a>
                                                </li>
                                                <li><a data-toggle="tooltip" data-placement="bottom"
                                                       data-original-title="Votes"><span>0</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-10 col-sm-10  col-xs-12">
                                            <p>On the other hand, we denounce with righteous indignation and dislike men
                                                who are so beguiled and demoralized by the charms of pleasure of the
                                                moment.</p>
                                            <div className="pull-right tagcloud"><a href="">Php</a> <a
                                                href="">recursive</a> <a href="">error</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="text-center clearfix">
                                <button className="btn btn-primary btn-lg">View All Question</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="clearfix"></div>
            </div>
        </div>
    </section>

);

export default Latest
