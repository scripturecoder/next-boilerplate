import Link from 'next/link'

const Top = () => (
    <div className="top-bar">
        <div className="container">
            <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-6 col-xs-4">
                    <ul className="top-nav nav-left">
                        <li><Link href="/"><a>Home</a></Link></li>
                        <li className="hidden-xs"><Link href="/articles"><a>Articles</a></Link></li>
                    </ul>
                </div>
                <div className="col-lg-8 col-md-8 col-sm-6 col-xs-8">
                    <ul className="top-nav nav-right">
                        <li><Link href="/login"><a><i className="fa fa-lock" aria-hidden="true"/>Login</a></Link></li>
                        <li><Link href="/register"><a><i className="fa fa-user-plus" aria-hidden="true"></i>Sign up</a></Link></li>
                        {/*<li className="dropdown">
                            <a className="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"
                               data-animations="fadeInUp">
                                <img className="img-circle resize" alt="" src="images/authors/13.jpg"/>
                                <span className="hidden-xs small-padding">
								<span>Umair</span><i className="fa fa-caret-down"/>
							</span>
                            </a>
                            <ul className="dropdown-menu ">
                                <li><a href="profile.html"><i className=" icon-bargraph"></i> Dashboard</a></li>
                                <li><a href="profile-setting.html"><i className=" icon-gears"></i> Profile Setting</a>
                                </li>
                                <li><a href="question-list.html"><i className="icon-heart"></i> Questions</a></li>
                                <li><a href="#"><i className="icon-lock"></i> Logout</a></li>
                            </ul>
                        </li>*/}
                    </ul>
                </div>
            </div>
        </div>
    </div>

);

export default Top
