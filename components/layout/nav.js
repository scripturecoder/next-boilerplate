import Link from 'next/link'


const Nav = () => (
    <div className="navbar navbar-default">
        <div className="container">
            <div className="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" className="navbar-toggle" type="button">
                    <span className="icon-bar"/>
                    <span className="icon-bar"/>
                    <span className="icon-bar"/>
                </button>
                <a href="index.html" className="navbar-brand">
                    <img className="img-responsive" alt="" src="/static/images/logo.png"/>
                </a>
            </div>
            <div className="navbar-collapse collapse">
                <ul className="nav navbar-nav navbar-right">
                    <li className="hidden-sm"><Link href="/login"><a>How It Works</a></Link></li>
                    <li><Link href="/questions"><a>Browse Questions</a></Link></li>
                    <li>
                        <div className="btn-nav"><a href="post-question.html" className="btn btn-primary btn-small navbar-btn">Post Question</a></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
);

export default Nav
