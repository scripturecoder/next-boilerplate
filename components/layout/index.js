import Link from 'next/link'
import Head from './head'
import Nav from './nav'
import Top from "./top";
import Footer from "./footer";

const Layout = (props) => (
    <div>
        <Head title="Home" />
        <Top/>
        <Nav />
        {props.children}
        <Footer/>
    </div>
);

export default Layout
