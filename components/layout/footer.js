import Link from 'next/link'

const Footer = () => (
    <footer className="footer-area">
        <div className="footer-content">
            <div className="container">
                <div className="row clearfix">
                    <div className="col-md-8 col-md-offset-2">
                        <div className="footer-content text-center no-padding margin-bottom-40">
                            <div className="logo-footer">
                                <img id="logo-footer" className="center-block" src="images/logo-1.png" alt=""/>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus illo vel dolorum
                                soluta consectetur doloribus sit. Delectus non tenetur odit dicta vitae debitis suscipit
                                doloribus. Lorem ipsum dolor sit amet, illo vel.</p>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-12 col-xs-12">
                        <div className="row clearfix">
                            <div className="col-lg-7 col-sm-6 col-xs-12 column">
                                <div className="footer-widget about-widget">
                                    <h2>Our Addres</h2>
                                    <ul className="contact-info">
                                        <li><span className="icon fa fa-map-marker"/> 60 Link Road Lhr. Pakistan
                                            54770
                                        </li>
                                        <li><span className="icon fa fa-phone"/> (042) 1234567890</li>
                                        <li><span className="icon fa fa-map-marker"/> contant@scriptsbundle.com
                                        </li>
                                        <li><span className="icon fa fa-fax"/> (042) 1234 7777</li>
                                    </ul>
                                    <div className="social-links-two clearfix">
                                        <a href="#" className="facebook img-circle">
                                            <span className="fa fa-facebook-f"/>
                                        </a>
                                        <a href="#" className="twitter img-circle">
                                            <span className="fa fa-twitter"/>
                                        </a>
                                        <a href="#" className="google-plus img-circle">
                                            <span className="fa fa-google-plus"/>
                                        </a>
                                        <a href="#" className="linkedin img-circle">
                                            <span className="fa fa-pinterest-p"/>
                                        </a>
                                        <a href="#" className="linkedin img-circle">
                                            <span className="fa fa-linkedin"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-5 col-sm-6 col-xs-12 column">
                                <h2>Our Service</h2>
                                <div className="footer-widget links-widget">
                                    <ul>
                                        <li><a href="#">Web Development</a>
                                        </li>
                                        <li><a href="#">Web Designing</a>
                                        </li>
                                        <li><a href="#">Android Development</a>
                                        </li>
                                        <li><a href="#">Theme Development</a>
                                        </li>
                                        <li><a href="#">IOS Development</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-12 col-xs-12">
                        <div className="row clearfix">
                            <div className="col-lg-7 col-sm-6 col-xs-12 column">
                                <div className="footer-widget news-widget">
                                    <h2>Latest News</h2>
                                    <div className="news-post">
                                        <div className="icon"></div>
                                        <div className="news-content">
                                            <figure className="image-thumb">
                                                <img src="images/blog/popular-2.jpg" alt=""/>
                                            </figure>
                                            <a href="#">If you need a crown or lorem an implant you will pay it gap
                                                it</a>
                                        </div>
                                        <div className="time">July 2, 2014</div>
                                    </div>
                                    <div className="news-post">
                                        <div className="icon"></div>
                                        <div className="news-content">
                                            <figure className="image-thumb">
                                                <img src="images/blog/popular-1.jpg" alt=""/>
                                            </figure>
                                            <a href="#">If you need a crown or lorem an implant you will pay it gap
                                                it</a>
                                        </div>
                                        <div className="time">July 2, 2014</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-5 col-sm-6 col-xs-12 column">
                                <div className="footer-widget links-widget">
                                    <h2>Site Links</h2>
                                    <ul>
                                        <li><a href="login.html">Login</a>
                                        </li>
                                        <li><a href="register.html">Register</a>
                                        </li>
                                        <li><a href="listing.html">Listing</a>
                                        </li>
                                        <li><a href="blog.html">Blog</a>
                                        </li>
                                        <li><a href="contact.html">Contact Us</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="footer-copyright">
            <div className="auto-container clearfix">
                <div className="copyright text-center">Copyright 2016 &copy; Theme Created By <a target="_blank" href="http://themeforest.net/user/scriptsbundle/portfolio">Scriptsbundle</a> All
                    Rights Reserved
                </div>
            </div>
        </div>
    </footer>


);

export default Footer
