import Layout from "../components/layout";
import React from "react";
import Index from "../components/home";

const Home = () => (
    <Layout>
        <Index/>
    </Layout>
);

export default Home
