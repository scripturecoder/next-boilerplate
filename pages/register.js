import Layout from "../components/layout";

const Register = () => (
    <Layout>
        <section className="page-title">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-sm-7 co-xs-12 text-left">
                        <h1>Create Account</h1>
                    </div>
                    <div className="col-md-6 col-sm-5 co-xs-12 text-right">
                        <div className="bread">
                            <ol className="breadcrumb">
                                <li><a href="#">Home</a>
                                </li>
                                <li><a href="#">Pages</a>
                                </li>
                                <li className="active">SIgn Up</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className="section-padding-80 white" id="register">
            <div className="container">
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">

                        <div className="box-panel">

                            <a href="#" className="btn btn-default facebook"><i className="fa fa-facebook icons"/> Sign Up with Facebook</a>
                            <a href="#" className="btn btn-default google"><i className="fa fa-google-plus icons"/> Sign Up with Google</a>

                            <p className="text-center margin-top-10"><span className="span-line">OR</span></p>

                            <form>
                                <div className="form-group">
                                    <label>Name</label>
                                    <input type="text" placeholder="Your Full Name" className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label>Email</label>
                                    <input type="email" placeholder="Your Email" className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <input type="password" placeholder="Your Password" className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" placeholder="Verify Your Password" className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <div className="row">

                                        <div className="col-xs-12 text-right">
                                            <p className="help-block"><a data-toggle="modal" href="login.html">Already
                                                Register Sing In</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <button className="btn btn-primary btn-lg btn-block">Create Account</button>

                            </form>

                        </div>
                    </div>

                    <div className="clearfix"/>
                </div>
            </div>
        </section>
    </Layout>
);

export default Register
