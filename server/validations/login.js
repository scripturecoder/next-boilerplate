module.exports = (req, res, next)=> {
    req.check('email', "Email is required").isEmail();
    req.check('password', "Password is required").not().isEmpty();

    const errors = req.validationErrors();
    if (errors) {
        return res.status(422).json({ errors: errors });
    }else {
        req.body.email= req.body.email.toLowerCase();
        next();
    }

};
