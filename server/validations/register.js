const User = require('../models/User');

module.exports = (req, res, next)=> {

    const {body} = req;
    req.check('email', "A valid email is required").isEmail();
    req.check('country_id', "Kindly select your country").not().isEmpty();

    const errors = req.validationErrors();
    if (errors) {
        return res.status(422).json({ errors: errors });
    }else {
        User.find({email:body.email}).then(user => {
            console.log(user);
            if (user.length > 0){
                return res.status(422).json({
                    success: false,
                    message: "E-mail already in use!"
                })
            }else{
                req.body.email= req.body.email.toLowerCase();
                next();
            }
        });
    }

};
