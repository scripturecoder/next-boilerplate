const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const crypto = require('crypto');

const TokenSchema = new mongoose.Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    time:{
        type:Date,
    },
    token:{
        type:String,
        default: crypto.randomBytes(100).toString('hex')
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    deleted: {
        type: Boolean,
        default: false
    },
});
module.exports = mongoose.model('Token', TokenSchema);
